--[[
È
]]--
local addonName, vars = ...
local L = vars.L --- localizzazione
local OT = _G[addonName]

--Upvalue
local format, tconcat, tinsert, tsort, UnitClass, UnitExists, IsInGroup, GetNumGroupMembers, UnitGroupRolesAssigned = string.format, table.concat, table.insert, table.sort, UnitClass, UnitExists, IsInGroup, GetNumGroupMembers, UnitGroupRolesAssigned

function OT:ClassColoredName(unit)
   local _,class = UnitClass(unit)
   local color = RAID_CLASS_COLORS[class]
   return(format("\|c%s%s\|r", color.colorStr,GetUnitName(unit)))
end

--ritorna una tabella contente nelle prime 3 posizioni numero contato, dps totali e morti.
function OT:TargetingList(what, inverse)
	--what puo' essere focus o target. altri valori sono errori di codice
	if not what or (what ~= "focus" and what ~= "target") then return {format("\|cFFFF0000 Error what = %s\|r",what or "nil")}  end
	--se non c'è trget o focus lo diciamo
	if not UnitExists(what) then return {format("\|cFFFF0000No %s\|r",what)}  end
	local where = OT:IsGrouped()
	--se non siamo in gruppo il frame non serve
	if not where then 
	  return {L["\|cFFFF0000Not in group\|r"]}
	end   
	local role,who   
	local m = GetNumGroupMembers()
	local n, dps,dead = 0 , 0 , 0
	local list = {}
	for i = 1, m do
	  who = where..i
	  if where == "party" and i == m then who = "player"end
	  role = UnitGroupRolesAssigned(who)         
	  if role ~= "TANK" and role ~= "HEALER" then
		 if not UnitIsDead(who) then
		   dps = dps+1		   
			if (not inverse and UnitIsUnit(what,who.."target")) or (inverse and not UnitIsUnit(what,who.."target")) then
			   n = n + 1			   
			   tinsert(list, OT:ClassColoredName(who))
			end
		 else
			dead = dead+1			
		 end
	  end      
	end

	if n == 0 then tinsert(list, L["\|cFFFF0000NO ONE\|r"]) end
	tsort(list)
	return list,n, dps,dead
end

function OT:DpsCountOn(what)
	--crea la lista solo se il frame è visibile    
	if OT[what] and OT[what]:IsShown() then 
		local inverse = OT:AssistMonitorListOrder(what)	
		local list, n, dps, dead = OT:TargetingList(what, inverse)
		list = tconcat(list,"\n")	
		if n then 
			OT[what].txt:SetFormattedText("%s%2d\/%2d DPS -%2d \|T%s:0\|t%s\n%s", not inverse and "" or "\|cFFFF0000", n, dps, dead, OT:GetTexture("Lapide"), not inverse and "" or "\|r", list)			
		else
			OT[what].txt:SetFormattedText("%s",list)
		end
	end
end


function updateOrderArrow(what)
	local arrow = OT:AssistMonitorListOrder(what) and "NotOn" or "ON"
	OT[what].title.arrow.texture:SetTexture(OT:GetTexture(arrow))
end

--crea ed inserisce la freccia colorata dell'ordine
function addAssistMonitorOrderButton(what)
	local frame = OT[what].title
	frame.arrow = CreateFrame("Frame",nil,frame)
	frame.arrow:SetSize(20,1)
	frame.arrow:SetPoint("TOPRIGHT",frame,"TOPRIGHT")	
	frame.arrow:SetPoint("BOTTOMRIGHT",frame,"BOTTOMRIGHT")				
	frame.arrow:EnableMouse(true)
	frame.arrow:SetScript("OnMouseUp", function(self)         
			OT:AssistMonitorToggleListOrder(what)
		end)			
	frame.arrow.texture = frame.arrow:CreateTexture(nil,"BACKGROUND",-6)
	frame.arrow.texture:SetAllPoints()
	updateOrderArrow(what)
end

--aggiorna la lista
function OT:AssistMonitorUpdateList(what)--what qui è un frame
   self:SetFrameVal(what, "counter", 0)
   self:DpsCountOn(OT:GetFrameVal(what,"type"))
end

function CreateAssistMonitor(what)
	if not OT[what] then
		local monitor = {closable=false,class = OT.monitorClass,type=what,savePos=true,title = OT:Upper1(what),titleColor = "g",}
		OT[what] = OT:Frame(monitor)
		addAssistMonitorOrderButton(what)--inserisce il pulsante per settare l'ordine di visualizzazione
		OT:SetFrameVal(OT[what],"counter", 0) 		
		OT[what]:SetScript("OnUpdate", function(self, elapsed)           
            OT:SetFrameVal(self, "counter", OT:GetFrameVal(self, "counter") + elapsed)
            if OT:GetFrameVal(self, "counter") > OT.delay then
               --OT:SetFrameVal(self, "counter", 0)
               --OT:DpsCountOn(OT:GetFrameVal(self,"type"))
					OT:AssistMonitorUpdateList(self)
            end
		end)       
	end      
end

local function GetAssistMonitorDB()
	return OT:DB()["assistMonitor"]
end

function OT:AssistMonitorToggleListOrder(what)
		--print(what)
		GetAssistMonitorDB()[what].inverti = not self:AssistMonitorListOrder(what)
		if OT[what] then 
			updateOrderArrow(what)
			OT:AssistMonitorUpdateList(OT[what])
		end
		LibStub("AceConfigRegistry-3.0"):NotifyChange(addonName)
end

function OT:ShowAssistMonitor(what)
	if what and (what == "focus" or what == "target") then
		if not OT[what] then CreateAssistMonitor(what) end
		OT[what]:Show()
	end
end



function OT:AssistMonitorListOrder(what)
	return GetAssistMonitorDB()[what].inverti
end

function OT:HideAssistMonitor(what)
	if what and (what == "focus" or what == "target") and OT[what] then    
		OT[what]:Hide()
	end
end

function OT:IsMonitorEnabled(what)
	if what then    
		return GetAssistMonitorDB()[what].attiva
	end
end

function OT:IsMonitorRaidOnly(what)
	if what then    
		return GetAssistMonitorDB()[what].raidOnly
	end
end

function OT:IsMonitornotLFR(what)
	if what then    
		return GetAssistMonitorDB()[what].notInLFR
	end
end

function OT:IsMonitorTest(what)
	if what and (what == "focus" or what == "target") then    
		return OT["_"..what.."MonitorTest"]
	end
end

function OT:CanMonitorBeShown(what)
	if OT:IsMonitorEnabled(what) and (
		(IsInGroup() or IsInRaid())
		and (not OT:IsMonitorRaidOnly(what) or IsInRaid())
		and (not OT:IsMonitornotLFR(what) or (select(3, GetInstanceInfo())) ~= 17)
	)
	then return true
	end
end

function OT:IsMonitorAlwaysOn(what)
	if what and (what == "focus" or what == "target") then    
		return GetAssistMonitorDB()[what].alwaysOn and OT:IsMonitorEnabled(what)
	end
end

function OT:AssistMonitorToggle(what)
	local attivo = OT:IsMonitorEnabled(what)
	if attivo and ((UnitAffectingCombat("player") and OT:CanMonitorBeShown(what)) or OT:IsMonitorAlwaysOn(what)) then							
			OT:ShowAssistMonitor(what)
	else
		OT:HideAssistMonitor(what)
	end
end

function OT:AssistMonitorEnableDisable(what)
	GetAssistMonitorDB()[what].attiva = not GetAssistMonitorDB()[what].attiva
	OT:AssistMonitorToggle(what)
end
