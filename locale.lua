--[[ custom locale file for OnTargets
	CREDITS
	
]]--
local addonName, vars = ...
local Ld, La = {}, {}
local locale = GAME_LOCALE or GetLocale()

vars.L = setmetatable({},{
    __index = function(t, s) return La[s] or Ld[s] or rawget(t,s) or s end
})
--starts from here
Ld["%s Monitor"] = "%s Monitor"
Ld["Apri Pannello delle opzioni"] = "Open the Options"
Ld["Assist Monitor"] = "Assist Monitor"
Ld["Attiva il monitor per il %s"] = "Show the %s monitor"
Ld["Attiva il monitor solo in raid."] = "Show the monitor in raid only."
Ld["Attualmente mostra %ssul %s|r"] = "Now showing %son the %s|r"
Ld["Invece di mostrare chi è sul %s, mostra chi deve ancora targettarlo."] = "Instead of showing who's on %s, shows \|cFFFF0000who still have to target it\|r."
Ld["Mostra chi sta attaccando il tuo target o il tuo focus"] = "Show who's attacking your target or your focus."
Ld["Mostra il numero dei dps sul tuo %s, il totale dei dps vivi e una lista dei loro nomi."] = "Show how many dps are targeting your %s, how many are still alive and their names."
Ld["Nascondi il monitor nei gruppi Ricerca delle Incursioni."] = "Don't show this monitor in a Raid Finder group."
Ld["Non nel Raid Finder"] = "Not in the Raid Finder"
Ld["OnTargets Options"] = "OnTargets Options"
Ld["Opzioni per il monitor del %s"] = "%s Monitor Options"
Ld["Puoi usare anche il comando \|cFFFF0000/ot|r."] = "You can also use the command \|cFFFF0000/ot|r."
Ld["Segna chi \|cFFFF0000NON|r è sul %s"] = "Show who is \|cFFFF0000NOT|r on your %s"
Ld["Solo in raid"] = "Only in raid"
Ld["Test mode"] = "Show always"
Ld["\|cFFFF0000NO ONE\|r"] = "\|cFFFF0000NO ONE\|r"
Ld["\|cFFFF0000Not in group\|r"] = "\|cFFFF0000Not in group\|r"
Ld["test mode desc"] = "Always show the monitor, useful mainly to set up its position."
Ld["|cFF00FF00chi è "] = "|cFF00FF00who is "
Ld["|cFFFF0000chi NON è "] = "|cFFFF0000who is not "
if locale == "frFR" then do end
--La["what"] = "quoi"
elseif locale == "deDE" then do end
--La["what"] = "was"
elseif locale == "koKR" then do end
elseif locale == "esMX" then do end
--La["what"] = "qué"
elseif locale == "ruRU" then do end
elseif locale == "zhCN" then do end
--La["what"] = "what_in_zh"
elseif locale == "esES" then do end
--La["what"] = "qué"
elseif locale == "zhTW" then do end
--La["what"] = "what_in_zh"
elseif locale == "ptBR" then do end
--La["what"] = "what_in_pt"
elseif locale == "itIT" then do end
La["%s Monitor"] = "%s Monitor"
La["Apri Pannello delle opzioni"] = "Apri Pannello delle opzioni"
La["Assist Monitor"] = "Assist Monitor"
La["Attiva il monitor per il %s"] = "Attiva il monitor per il %s"
La["Attiva il monitor solo in raid."] = "Attiva il monitor solo in raid."
La["Attualmente mostra %ssul %s|r"] = "Attualmente mostra %ssul %s|r"
La["Invece di mostrare chi è sul %s, mostra chi deve ancora targettarlo."] = "Invece di mostrare chi è sul %s, mostra chi deve ancora targettarlo.\|r."
La["Mostra chi sta attaccando il tuo target o il tuo focus"] = "Mostra chi sta attaccando il tuo target o il tuo focus."
La["Mostra il numero dei dps sul tuo %s, il totale dei dps vivi e una lista dei loro nomi."] = "Mostra il numero dei dps sul tuo %s, il totale dei dps vivi e una lista dei loro nomi."
La["Nascondi il monitor nei gruppi Ricerca delle Incursioni."] = "Nascondi il monitor nei gruppi Ricerca delle Incursioni."
La["Non nel Raid Finder"] = "Non nel Raid Finder"
La["OnTargets Options"] = "OnTargets Options"
La["Opzioni per il monitor del %s"] = "Opzioni per il monitor del %s"
La["Puoi usare anche il comando \|cFFFF0000/ot|r."] = "Puoi usare anche il comando \|cFFFF0000/ot|r."
La["Segna chi \|cFFFF0000NON|r è sul %s"] = "Segna chi \|cFFFF0000NON|r è sul %s"
La["Solo in raid"] = "Solo in raid"
La["Test mode"] = "Mostra sempre"
La["\|cFFFF0000NO ONE\|r"] = "\|cFFFF0000NESSUNO\|r"
La["\|cFFFF0000Not in group\|r"] = "\|cFFFF0000Non in gruppo\|r"
La["test mode desc"] = "Mostra sempre il monitor, utile principalmente per decidere la sua posizione."
La["|cFF00FF00chi è "] = "|cFF00FF00chi è "
La["|cFFFF0000chi NON è "] = "|cFFFF0000chi NON è "
end
