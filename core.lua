--[[
	OnTargets
	
	Controlla chi è sul target e chi è sul focus (o chi NON è). 
	
	by Stormbeard - Pozzo Dell'Eternità, gilda Usque ad Finem
]]---
local addonName, vars = ...
local L = vars.L --- localizzazione
local OT = LibStub("AceAddon-3.0"):NewAddon(addonName, "AceTimer-3.0","AceEvent-3.0")
_G[addonName] = OT


--***************************************
--<UPVALUE>
local pairs = pairs
local ipairs = ipairs
local print = print
local unpack = unpack
local type = type
local next = next
local tinsert = table.insert
local tsort = table.sort
local tconcat = table.concat
local tremove = table.remove
local wipe = wipe
local format = string.format
local match = string.match
local find = string.find
local lower = string.lower
local upper = string.upper
local gsub = gsub
local gmatch = string.gmatch
local strtrim = strtrim
local setmetatable = setmetatable
local select = select
local IsInRaid = IsInRaid
local IsInGroup = IsInGroup
local UnitPlayerOrPetInRaid = UnitPlayerOrPetInRaid
local UnitGUID = UnitGUID
local GetNumGroupMembers = GetNumGroupMembers
local UnitIsUnit = UnitIsUnit
local GetUnitName = GetUnitName
local UnitName = UnitName
local UnitAffectingCombat = UnitAffectingCombat

local _


--</UPVALUE>

--***************************************
--<VARIABILI>
local db = {}
	
local DEFAULTDB = {
	profile={		
		assistMonitor = {
			focus = {},
			target = {
				["attiva"] = true,
			},
		},
	},
}
--</VARIABILI>
--<UTILITY>
--works as pair but sorting keys

local function pairsByKeys (t, criterio)
      local sorted = {}
      for n in pairs(t) do tinsert(sorted, n) end
      tsort(sorted, criterio)
      local i = 0      -- iterator variable
      local iter = function ()   -- iterator function
        i = i + 1
        if sorted[i] == nil then return nil
        else return sorted[i], t[sorted[i]]
        end
      end
      return iter
end

OT.pairsByKeys = pairsByKeys
	
function copytable(obj, seen)
  if type(obj) ~= 'table' then return obj end
  if seen and seen[obj] then return seen[obj] end
  local s = seen or {}
  local res = setmetatable({}, getmetatable(obj))
  s[obj] = res
  for k, v in pairs(obj) do res[copytable(k, s)] = copytable(v, s) end
  return res
end

function OT:InRaid(unit)
	return UnitPlayerOrPetInRaid(unit) and find(UnitGUID(unit) or "", "Player")
end

--i maledetti nomi dei pg servono a volte con server a volte senza
function OT:GetName(name,returnserver,nameIsUnit)
	name = nameIsUnit and GetUnitName(name,returnserver) or name
	local n = name
	if not returnserver then
		n = match(n, "([^-]+)") --senza il server
	else
		local s
		n, s = match(n, "([^-]+)-*([^-]*)")
		s = s =="" and self.PGRealm or s		
		n = format("%s-%s",n,s)
		n = gsub(n,"%s","")
	end	
	 return n or ""
end

function OT:IsGrouped()
	local where = IsInGroup() and "party" or nil
	where = IsInRaid() and "raid" or where
	return where
end

function OT:Upper1(txt)
   return gsub(txt, "^%l", upper)
end

function OT:GetTexture(name)
   return format([[Interface\AddOns\%s\Textures\%s]],addonName,name)
end

--**************************


--shortcuts
function OT:DB()
	return OT.db.profile
end

function OT:Getopzioni(...)--(var)	
	return GetReturn[select("#",...)](OT.db.profile.opzioni,...)
end

function OT:Setopzioni(var, val)
	OT._tmpRef = OT:Getopzioni()	
	OT._tmpRef[var] = val
end

function OT:SetDbTable(name, ref)
	OT.db.profile[name] = ref
end

--***************************
function OT:SetTempVal(var, val)
	OT._tempVal[var] = val
end

function OT:GetTempVal(var)
	return OT._tempVal[var]
end

--************************--


--esegue una funzione, i vararg sono devono essere già passati come tabella
function OT:Run(func, args)
	func = type(func) == "function" and func or function()end
	args = args or {}
	args = type(args) ~= "table" and {args} or args
	func(unpack(args))
end

--controllo che l'incontro effettivamente finisca. piano B. ogni tanto encounter_end non viene intercettato.
function OT:PLAYER_REGEN_ENABLED()	
	local where = OT:IsGrouped()
	if where then 
		for i = 1, GetNumGroupMembers() do
			if UnitAffectingCombat(where..i) then
				--se ancora qualcuno si mena controlliamo tra un po'
				OT:ScheduleTimer("PLAYER_REGEN_ENABLED", 3)
				return
			end
		end				
	end
		--se siamo qui nessuno combatte più
	OT:UnregisterEvent("PLAYER_REGEN_ENABLED")
	OT:RegisterEvent("PLAYER_REGEN_DISABLED")

	if not OT:IsMonitorAlwaysOn("focus") then OT:HideAssistMonitor("focus") end
	if not OT:IsMonitorAlwaysOn("target") then OT:HideAssistMonitor("target") end
end

function OT:PLAYER_REGEN_DISABLED()
	if OT:CanMonitorBeShown("target") then OT:ShowAssistMonitor("target") end
	if OT:CanMonitorBeShown("focus") then OT:ShowAssistMonitor("focus") end
	OT:UnregisterEvent("PLAYER_REGEN_DISABLED")
	OT:RegisterEvent("PLAYER_REGEN_ENABLED")   
end

function OT:OnInitialize()
	  -- Code that you want to run when the addon is first loaded goes here.
	self.db = LibStub("AceDB-3.0"):New("OTDB", DEFAULTDB)
	db = self.db.profile
	self.db.RegisterCallback(self, "OnNewProfile", "ManageProfile")
	self.db.RegisterCallback(self, "OnProfileChanged", "ManageProfile")
	self.db.RegisterCallback(self, "OnProfileDeleted", "ManageProfile")
	self.db.RegisterCallback(self, "OnProfileCopied", "ManageProfile")
	self.db.RegisterCallback(self, "OnProfileReset", "ManageProfile")
	
	self.delay = 0.5 --ritardo con cui si eseguono i comandi, per evitare lockout
	OT._tempVal = {}
	self.monitorClass = "assistMonitor"	
end

function OT:OnEnable()	
    -- Called when the addon is enabled	
	
	self.maxLvl = GetMaxPlayerLevel()
	self.PGRealm = gsub(GetRealmName(),"%s","") 	
	--self:IniOptions()	
	OT:AddBlizOpt()

	OT:RegisterEvent("PLAYER_REGEN_DISABLED")
	if OT:IsMonitorAlwaysOn("focus") then OT:ShowAssistMonitor("focus") end
	if OT:IsMonitorAlwaysOn("target") then OT:ShowAssistMonitor("target") end	
end

function OT:OnDisable()
    -- Called when the addon is disabled
end

function OT:ManageProfile(event, DB, prof)
	db = self.db.profile		
	wipe(self.options)	
	self:IniOptions()	
end

--[[***************************************

SLASH COMMAND

******************************************]]

function OT:SlashCommand(msg)	
		OT:OpenOptions(tab)		
end

SLASH_ONTARGET1 ="/ontarget"
SLASH_ONTARGET2 ="/ot"
function SlashCmdList.ONTARGET(msg)
	OT:SlashCommand(msg)
end