--[[

]]--
local addonName, vars = ...
local L = vars.L --- localizzazione
local OT = _G[addonName]

--Upvalue
local CreateFrame, strlen = CreateFrame,strlen

--
--legge un valore d'appoggio settato in un frame
function OT:GetFrameVal(frame, val)
	--if not frame._custom then return end
	if frame.OT and frame.OT._custom then
		return frame.OT._custom[val]   
	end
end

--setta un valore d'appoggio in un frame
function OT:SetFrameVal(frame, var, val)
	frame.OT = frame.OT or {}
	frame.OT._custom = frame.OT._custom or {}
	frame.OT._custom[var] = val
end

function OT:GetFrameDB(frame, val)  
	local r = OT:DB()
	local class = OT:GetFrameVal(frame, "class")
	local type = OT:GetFrameVal(frame,"type") or 1
	if r[class] and r[class][type] then
		return r[class][type][val]   
	end   
end

function OT:SetFrameDB(frame, var, val)
	local r = OT:DB()
	local class = OT:GetFrameVal(frame, "class")
	local type = OT:GetFrameVal(frame,"type") or 1
	if r[class] and r[class][type] then
		r[class][type][var] = val
	end   
end

--se nel db e' presente un class[type] o un class[1] allora salva
function OT:EnableFrameDB(frame)
	local r = OT:DB()
	local class = OT:GetFrameVal(frame, "class")
	if not class then return end
	local type = OT:GetFrameVal(frame,"type") or 1
	if r[class] and r[class][type] then	
		r[class][type] = r[class][type] or {}
		OT:SetFrameVal(frame, "DBEnabled", true)
	end      
end

function OT:SaveFramePos(frame)
	if not OT:GetFrameVal(frame, "DBEnabled") then return end
	local from, _, to, x, y = frame:GetPoint()   
	OT:SetFrameDB(frame, "from", from)
	OT:SetFrameDB(frame,"to", to)
	OT:SetFrameDB(frame,"x",x)
	OT:SetFrameDB(frame,"y", y)
   
end

function OT:RestoreFramePos(frame)
   local from, parent, to, x, y = OT:GetFrameDB(frame, "from"),frame:GetParent(),OT:GetFrameDB(frame,"to"), OT:GetFrameDB(frame,"x"), OT:GetFrameDB(frame,"y")
   if from and parent and to and x and y then
      frame:ClearAllPoints()
      frame:SetPoint(from, parent, to, x, y)
   end   
end

function OT:LockFrame(frame)
	OT:SetFrameVal(frame,"lock", true)
	OT:SetFrameDB(frame,"lock",true)
	frame.title.lock:SetTexture(OT:GetTexture("lock"))
    frame:EnableMouse()
    frame.title.lockFrame:EnableMouse(true)	
	frame.bg:SetAlpha(0)
end

function OT:UnlockFrame(frame)
	OT:SetFrameVal(frame,"lock")
	OT:SetFrameDB(frame,"lock")
	frame.title.lock:SetTexture(OT:GetTexture("unlock"))
    frame:EnableMouse(true)
	frame.bg:SetAlpha(1)
end

function OT:LockUnlockFrame(frame,state)	
	if state then
		OT:LockFrame(frame)
	else      
		OT:UnlockFrame(frame)
	end   
end


--*********************************************
--*********************************************
local colors ={
	r = {1,0,0},
	g = {0,1,0},
	b = {0,0,1},	
	y = {1,1,0},
	c = {0.4,0.8,0.9},
	n = {0,0,0},
	w = {1,1,1},
}

setmetatable(colors, {__index=function()return {1,1,0}end,})

local function GetFrameVal(frame, val)
	--if not frame._custom then return end
	
	if frame.OT and frame.OT._custom then		
		return frame.OT._custom[val]   
	end
end

--setta un valore d'appoggio in un frame
local function SetFrameVal(frame, var, val)
	frame.OT = frame.OT or {}
	frame.OT._custom = frame.OT._custom or {}
	frame.OT._custom[var] = val	
end

local function GetFrameDB(frame, val)  
	local r = OT:DB()
	local class = GetFrameVal(frame, "class")
	local type = GetFrameVal(frame,"type") or 1
	if r[class] and r[class][type] then
		return r[class][type][val]   
	end   
end

local function SetFrameDB(frame, var, val)	
	local r = OT:DB()
	local class = GetFrameVal(frame, "class")
	local type = GetFrameVal(frame,"type") or 1
	if r[class] and r[class][type] then				
		r[class][type][var] = val
	end   
end

local function EnableFrameDB(frame)
	local r = OT:DB()
	local class = GetFrameVal(frame, "class")	
	if not class then return end
	local type = GetFrameVal(frame,"type") or 1	
	if r[class] and r[class][type] then
		r[class][type] = r[class][type] or {}
		SetFrameVal(frame, "DBEnabled", true)
	end      
end

local function SaveFramePos(frame)	
	if not GetFrameVal(frame, "DBEnabled") then return end
	local from, _, to, x, y = frame:GetPoint()  
	SetFrameDB(frame, "from", from)
	SetFrameDB(frame,"to", to)
	SetFrameDB(frame,"x",x)
	SetFrameDB(frame,"y", y)
   
end

local function RestoreFramePos(frame)
   local from, parent, to, x, y = GetFrameDB(frame, "from"),frame:GetParent(),GetFrameDB(frame,"to"), GetFrameDB(frame,"x"), GetFrameDB(frame,"y")
   if from and parent and to and x and y then
      frame:ClearAllPoints()
      frame:SetPoint(from, parent, to, x, y)
   end   
end

local function LockFrame(frame)
	SetFrameVal(frame,"lock", true)
	SetFrameDB(frame,"lock",true)
	frame.title.lock:SetTexture(OT:GetTexture("lock"))
    frame:EnableMouse()
    frame.title.lockFrame:EnableMouse(true)	
	frame.bg:SetAlpha(0)
end

local function UnlockFrame(frame)
	SetFrameVal(frame,"lock")
	SetFrameDB(frame,"lock")
	frame.title.lock:SetTexture(OT:GetTexture("unlock"))
    frame:EnableMouse(true)
	frame.bg:SetAlpha(1)
end

local function LockUnlockFrame(frame,state)	
	if state then
		LockFrame(frame)
	else      
		UnlockFrame(frame)
	end   
end

local frameProto = {
	movable = true,
	resizable = true,
	savePos = nil,
	title = true,
	lockable = true,
	closable = true,		
}

function frameProto:IsClosable()
	return self.closable
end

function frameProto:IsLockable()
	return self.lockable
end

function frameProto:GetTitle(txt)
	return self.frame.title.txt:GetText()
end

function frameProto:SetTitle(txt,color)	
	self.frame.title.txt:SetFormattedText("%s",txt)
	self.frame.title.txt:SetTextColor(unpack(colors[color]))
	local width = strlen(txt)*7+20+ (self:IsClosable() and 20 or 0) + (self:IsLockable() and 20 or 0)--7 è per la grandezza del font, andrebbe cambiato sulla base dell'effettiva grandezza
	self.frame:SetWidth(max(self.frame:GetWidth(), width))
end

function frameProto:SetFrameVal(var, val)	
	SetFrameVal(self.frame, var,val)
end

function frameProto:GetFrameVal(var)
	return GetFrameVal(self.frame, var)
end

function frameProto:EnableFrameDB()
	EnableFrameDB(self.frame)
end

function frameProto:RestoreFramePos()
	RestoreFramePos(self.frame)
end

function frameProto:LockUnlockFrame(state)
	LockUnlockFrame(self.frame,state)
end

function frameProto:SaveFramePos()
	SaveFramePos(self.frame)
end

function frameProto:RestoreFramePos()
	RestoreFramePos(self.frame)
end

function frameProto:SetFrameDB(var, val)
	SetFrameDB(self.frame, var, val)
end

function frameProto:GetFrameDB(var)
	return GetFrameDB(self.frame, var)
end

function frameProto:RestoreLock()
	LockUnlockFrame(self.frame, self:GetFrameDB("lock"))
end

function OT:Frame(pTable)
	ptable = pTable or {}			
	if pTable.name then
		if type(pTable.name) ~= "string" and pTable.class then
			pTable.name = format("%s%s%s", addonName, pTable.class, pTable.type or 1)
		end
	end
	local frame = CreateFrame('Frame',pTable.name,UIParent)  
	frame.OT = setmetatable({},{__index = frameProto,})
	frame.OT.frame = frame
	--i valori della tabella passata	
	for k, v in pairs(pTable or {}) do		
		frame.OT[k] = v
	end
	-----
	if frame.OT.class then			
		frame.OT:SetFrameVal("class", frame.OT.class)
		if frame.OT.type then			
			frame.OT:SetFrameVal("type", frame.OT.type)
		else
			frame.OT.type = "1"
			frame.OT:SetFrameVal("type", "1")
		end
	end				
	
	frame:SetSize(120,200)
	frame:SetPoint("TOP",UIParent, "TOP", 0, -50)
	frame.bg = frame:CreateTexture(nil, "BACKGROUND",nil,-7)
	frame.bg:SetAllPoints()
	frame.bg:SetColorTexture(0,0,0,0.2)
	frame:EnableMouse(true)
	frame:SetMovable(true)
	frame:SetResizable(true)
	frame:SetClampedToScreen(true)
	frame:SetFrameStrata("HIGH")

	if frame.OT.savePos then			
		if frame.OT:GetFrameVal("class") then			
			frame.OT:EnableFrameDB()			
			frame.OT:RestoreFramePos()
		else
			frame.OT.savePos = nil
		end
	end
	
	if frame.OT.movable then 
		frame:RegisterForDrag("LeftButton")
		frame:SetScript("OnDragStart", function(self)
			 if self:IsMovable() then
				self:StartMoving()
			 end
		end)
		frame:SetScript("OnDragStop", function(self)
			 self:StopMovingOrSizing() 
			 frame.OT:SaveFramePos()
		end)
		frame:SetScript("OnHide", function()
			 frame:StopMovingOrSizing()       
			 frame.OT:SaveFramePos()
		end)
	end
	
	if frame.OT.title then
		frame.title = CreateFrame("Frame",nil,frame)
		frame.title:SetSize(1,20)
		frame.title:SetPoint("TOPRIGHT",frame,"TOPRIGHT")
		frame.title:SetPoint("TOPLEFT",frame,"TOPLEFT")
		frame.title.bg = frame.title:CreateTexture(nil,"BACKGROUND", nil,-7)
		frame.title.bg:SetAllPoints()
		frame.title.bg:SetColorTexture(1,1,1,0.4)	
		
		if frame.OT.lockable then
			frame.title.lockFrame = CreateFrame("Frame",nil,frame.title)
			frame.title.lockFrame:SetSize(20,1)
			frame.title.lockFrame:SetPoint("TOPLEFT",frame.title,"TOPLEFT")		
			frame.title.lockFrame:SetPoint("BOTTOMLEFT",frame.title,"BOTTOMLEFT")		
			frame.title.lockFrame:EnableMouse(true)
			frame.title.lockFrame:SetScript("OnMouseUp", function(self)         
					OT:LockUnlockFrame(frame, not OT:GetFrameVal(frame,"lock"))
				end)
			
			frame.title.lock = frame.title.lockFrame:CreateTexture(nil,"BACKGROUND",-6)
			frame.title.lock:SetAllPoints()
			frame.title.lock:SetTexture(OT:GetTexture("unlock"))
			frame.OT:RestoreLock()			
		end
			
		frame.title.txt = frame.title:CreateFontString(nil, "BACKGROUND")
		frame.title.txt:SetFontObject(GameFontNormal)
		frame.title.txt:SetTextColor(0, 1, 0)
		frame.title.txt:SetPoint("TOPRIGHT",frame.title,"TOPRIGHT")
		frame.title.txt:SetPoint("BOTTOMRIGHT",frame.title,"BOTTOMRIGHT")
		frame.title.txt:SetPoint("TOPLEFT",frame.title,"TOPLEFT")
		if type(frame.OT.title) == "string" then
			frame.OT:SetTitle(frame.OT.title,frame.OT.titleColor)
		end

		frame.txt = frame:CreateFontString(nil, "BACKGROUND")
		frame.txt:SetFontObject(GameFontNormal)
		frame.txt:SetTextColor(1, 1, 1)
		frame.txt:SetPoint("TOPLEFT",frame.title,"BOTTOMLEFT",0,-5)   
		frame.txt:SetPoint("TOPRIGHT",frame.title,"BOTTOMRIGHT",0,-5)   
		frame.txt:SetWordWrap(true)
		
		if frame.OT.closable then
			frame.title.close = CreateFrame("Button",nil, frame, "UIPanelCloseButton")
			frame.title.close:SetSize(20,1)
			frame.title.close:SetPoint("TOPRIGHT",frame.title,"TOPRIGHT")		
			frame.title.close:SetPoint("BOTTOMRIGHT",frame.title,"BOTTOMRIGHT")		
		end
	else	
		frame.OT.lockable = nil
		frame.OT.closable = nil
	end
	  
	return frame
end