--[[


]]--
local addonName, vars = ...
local L = vars.L --- localizzazione
local OT = _G[addonName]
local _
local pairsByKeys = OT.pairsByKeys

local HideUIPanel, IsInGuild, pairs,next,format, UnitAffectingCombat = HideUIPanel, IsInGuild, pairs,next,string.format,UnitAffectingCombat

--<GET/SET Generici>
--get/set a seconda di #info (il nodo nell'interfaccia delle opzioni)
local GET = {
	[1] = function(info) return OT.db.profile[info[#info]]end,
	[2] = function(info) return OT.db.profile[info[#info-1]][info[#info]]end,
	[3] = function(info) return OT.db.profile[info[#info-2]][info[#info-1]][info[#info]]end,
	[4] = function(info) return OT.db.profile[info[#info-3]][info[#info-2]][info[#info-1]][info[#info]]end,
	[5] = function(info) return OT.db.profile[info[#info-4]][info[#info-3]][info[#info-2]][info[#info-1]][info[#info]]end,
}

local SET = {
	[1] = function(info, val) OT.db.profile[info[#info]] = val end,
	[2] = function(info, val) OT.db.profile[info[#info-1]][info[#info]] = val end,
	[3] = function(info, val) OT.db.profile[info[#info-2]][info[#info-1]][info[#info]] = val end,
	[4] = function(info, val) OT.db.profile[info[#info-3]][info[#info-2]][info[#info-1]][info[#info]] = val end,
	[5] = function(info, val) OT.db.profile[info[#info-4]][info[#info-3]][info[#info-2]][info[#info-1]][info[#info]] = val end,
}
--OPTIONS GET
function OT:DBGet(info)
	return GET[#info](info)
end

--OPTIONS SET
function OT:DBSet(info, val)
	SET[#info](info, val)
end

--funzioncina per creare un po' di spazio nelle opzioni
local function space(order, size)
	return {
		order = order,
		type = "description",
		name = "",
		width = size or "full",
	}
end

--ordina la lista dei pulsanti nelle opzioni
function OT:SortView(tab)
	if not self.options then return end
	if not next(tab) then return end
	local i = 1
	for name in pairsByKeys(tab) do	
		tab[name].order = i
		i = i+1
	end
end
--

local function TargetOrFocusOptions(what)
	if what and (what =="focus" or what =="target") then
		local opt =	{
			type = "group",
			order = what == "focus" and 20 or 10,
			name =  format(L["%s Monitor"],OT:Upper1(what)),
			desc = format(L["Opzioni per il monitor del %s"],OT:Upper1(what)),						
			inline = true,
			args = {
				attiva = {
					type = "toggle",
					order = 10,
					name = format(L["Attiva il monitor per il %s"],OT:Upper1(what)),	
					desc = format(L["Mostra il numero dei dps sul tuo %s, il totale dei dps vivi e una lista dei loro nomi."],what),	
					width = "full",
					set = function(info,val)
						OT:DBSet(info,val)
						OT:AssistMonitorToggle(info[#info-1])
						end,
					},
					_status = {
						type = "description",
						order = 20,
						fontSize = "medium",
						name = function(info) return format(L["Attualmente mostra %ssul %s|r"], OT:AssistMonitorListOrder(info[#info-1]) and L["|cFFFF0000chi NON è "] or L["|cFF00FF00chi è "], what)end,
						image = function(info) 
							local arrow = OT:AssistMonitorListOrder(info[#info-1]) and "NotOn" or "ON"
							return OT:GetTexture(arrow)
						end,
						imageWidth = 20,
						imageHeight = 20,						
					}, 
					inverti = {
						type = "toggle",
						order = 30,
						name = format(L["Segna chi \|cFFFF0000NON|r è sul %s"],	what),
						desc = format(L["Invece di mostrare chi è sul %s, mostra chi deve ancora targettarlo."],what),
						width = "full",
						set = function(info, state)
							--OT:DBSet(info, state)
							OT:AssistMonitorToggleListOrder(info[#info-1])
						end,
						disabled = function (info) return not OT:IsMonitorEnabled(info[#info-1]) end,
					}, 
				raidOnly = {
					type = "toggle",
					order = 40,
					name = L["Solo in raid"],	
					desc = L["Attiva il monitor solo in raid."],						
					disabled = function (info) return not OT:IsMonitorEnabled(info[#info-1]) end,
					}, 
					notInLFR = {
					type = "toggle",
					order = 50,
					name = L["Non nel Raid Finder"],	
					desc = L["Nascondi il monitor nei gruppi Ricerca delle Incursioni."],	
					disabled = function (info) return not OT:IsMonitorEnabled(info[#info-1]) end,					
					}, 							
				alwaysOn = {
					type = "toggle",
					order = 100,
					name = L["Test mode"],
					desc = L["test mode desc"],	
					width = "full",					
					set = function(info,val)
						OT:DBSet(info,val)
						OT:AssistMonitorToggle(info[#info-1])
					end,
					disabled = function (info) return not OT:IsMonitorEnabled(info[#info-1]) end,
				},						
			},
		}
		return opt
	end
end

function OT:AddBlizOpt()
	local options = { 
		name = L["OnTargets Options"],
		type = "group",
		handler = OT,				
		childGroups ="tab",
		args = {
			open = {
				order = 5,
				type = "execute",
				name = L["Apri Pannello delle opzioni"],				
				width = "double",
				func = function() 
						InterfaceOptionsFrame:Hide()                 
						HideUIPanel(GameMenuFrame)
						--LibStub("AceConfigDialog-3.0"):Open(addonName)
						OT:OpenOptions()
					end
				},		
			hint = {
				order = 10,
				type = "description",
				name = L["Puoi usare anche il comando \|cFFFF0000/ot|r."],
				width = "full",
				fontSize = "large",
			},
		},		
	}
	LibStub("AceConfig-3.0"):RegisterOptionsTable("BlizOpt", options)	
	self.optionsFrame = LibStub("AceConfigDialog-3.0"):AddToBlizOptions("BlizOpt", "OnTargets")	
end

function OT:OpenOptions(tab)
	if not self.options then OT:IniOptions() end
	LibStub("AceConfigDialog-3.0"):Open(addonName)	
	if tab then
		LibStub("AceConfigDialog-3.0"):SelectGroup(addonName, tab)
	end
end

function OT:IniOptions()
	
	local options = { 
		name = L["OnTargets Options"],
		type = "group",
		handler = OT,		
		get= "DBGet",
		set= "DBSet",	
		childGroups ="tab",
		args = {
			assistMonitor = {
				type = "group",
				order = 70,
				name = L["Assist Monitor"],
				desc = L["Mostra chi sta attaccando il tuo target o il tuo focus"],
				args = {				
					target = TargetOrFocusOptions("target"),											
					focus = TargetOrFocusOptions("focus"),
				},
			},			
			profiles = LibStub("AceDBOptions-3.0"):GetOptionsTable(OT.db),
		},
	}
	self.options = options	
	LibStub("AceConfig-3.0"):RegisterOptionsTable(addonName, options)	
	
end