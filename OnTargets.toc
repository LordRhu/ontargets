## Interface: 70300
## Title: On Targets
## Author: Stormbeard
## Notes: |cFF00FF00Check how many DDs are targeting your target or your focus|r
## SavedVariables: OTDB
## OptionalDeps: Ace3
## X-Embeds: Ace3


#@no-lib-strip@
embeds.xml
#@end-no-lib-strip@
libs\LibDataBroker-1.1\LibDataBroker-1.1.lua

locale.lua
core.lua
frame.lua
assistMonitor.lua
options.lua
